<?php
/**
 * @class
 * Class file.
 */

interface PDOSnapshotConnectionExtenderInterface {
  function __construct(DatabaseConnection $connection);
  function getTableNames();
  function lockDBForDump();
  function unlockDBForDump();
  function lockDBForRestore();
  function unlockDBForRestore();
}