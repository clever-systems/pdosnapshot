<?php
/**
 * @class
 * Class file.
 */

class PDOSnapshotConnectionExtenderSqlite extends PDOSnapshotConnectionExtenderBase implements PDOSnapshotConnectionExtenderInterface {
  protected $transaction;

  function getTableNames() {
    $query ='SELECT name FROM sqlite_master WHERE type=\'table\';';
    $table_names = $this->connection->query($query)->fetchCol();
    $excluded_tables = array('sqlite_sequence');
    $table_names = array_diff($table_names, $excluded_tables);
    return $table_names;
  }
}
