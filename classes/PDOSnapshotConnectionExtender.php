<?php
/**
 * @class
 * Class file.
 */

abstract class PDOSnapshotConnectionExtender {
  static function get($connection) {
    $type = ucfirst($connection->databaseType());
    $class = "PDOSnapshotConnectionExtender$type";
    /** @var PDOSnapshotConnectionExtenderInterface $return */
    $return = new $class($connection);
    return $return;
  }
}
