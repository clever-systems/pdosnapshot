<?php
/**
 * @class
 * Class file.
 */

class PDOSnapshotConnectionExtenderMysql extends PDOSnapshotConnectionExtenderBase implements PDOSnapshotConnectionExtenderInterface {
  private $overridden;

  function getTableNames() {
    $query ='SHOW TABLES;';
    $table_names = $this->connection->query($query)->fetchCol();
    return $table_names;
  }
}
