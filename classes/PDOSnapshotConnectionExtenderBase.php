<?php
/**
 * @class
 * Class file.
 */

abstract class PDOSnapshotConnectionExtenderBase {
  protected $connection;
  protected $transaction;

  function __construct(DatabaseConnection $connection) {
    $this->connection = $connection;
  }

  /**
   * @return \DatabaseConnection
   */
  public function getConnection() {
    return $this->connection;
  }

  function lockDBForDump() {
    $this->lockDB();
  }
  function unlockDBForDump() {
    $this->unlockDB();
  }
  function lockDBForRestore() {
    $this->lockDB();
  }
  function unlockDBForRestore() {
    $this->unlockDB();
  }

  /**
   * A simple lock implementation that wraps our work in a transaction.
   *
   * @todo It might make sense to choose the locking strategy as a command option.
   */
  protected function lockDB() {
    $this->transaction = $this->connection->startTransaction();
  }

  protected function unlockDB() {
    unset($this->transaction);
  }
}
