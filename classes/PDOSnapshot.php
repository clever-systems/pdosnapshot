<?php

class PDOSnapshot {
  /** @var string $path */
  private $path;
  /** @var DatabaseConnection $connection */
  private $connection;
  /** @var callable $logger */
  private $logger = NULL;
  /** @var callable $micrologger */
  private $micrologger = NULL;
  /** @var callable $drop_table_content */
  private $is_cache_table = NULL;
  /** @var float[] $timing_info */
  private $timing_info = array();
  /** @var array[string]|null $prefixes */
  private $table_prefixes = NULL;

  public function __construct($connection, $path, $options) {
    $this->connection = $connection;
    $this->path = $path;

    if (isset($options['logger'])) {
      $this->logger = $options['logger'];
      assert(is_callable($this->logger));
    }
    if (isset($options['micrologger'])) {
      $this->micrologger = $options['micrologger'];
      assert(is_callable($this->micrologger));
    }
    if (isset($options['is_cache_table'])) {
      $this->is_cache_table = $options['is_cache_table'];
      assert(is_callable($this->is_cache_table));
    }
    if (isset($options['table_prefixes'])) {
      $this->table_prefixes = $options['table_prefixes'];
    }
  }

  public function dump() {
    $this->timingInfoStart('all');

    // Get lock.
    $this->timingInfoStart('lock');
    PDOSnapshotConnectionExtender::get($this->connection)->lockDBForDump();
    $elapsed = $this->timingInfo('lock');
    $this->log('Got lock in @elapsed s.', array('@elapsed' => $elapsed), 'ok');

    // Get the current schema. Be sure and rebuild.
    $schema = drupal_get_schema(NULL, TRUE);

    // Iterate all the tables in the schema.
    $processed_table_names = array();
    $total_count = count($schema);
    foreach ($schema as $table_name => $table_schema) {

      $processed_table_names[] = $table_name;
      $table_count = count($processed_table_names);

      $t_args = array('@table' => $table_name, '@count' => $table_count, '@total' => $total_count);
      if ($this->checkTablePrefix($table_name)) {
        $this->log('Dumping @count/@total: @table', $t_args, 'ok');
      }
      else {
        $this->log('Skipping due to prefix @count/@total: @table', $t_args, 'ok');
        continue;
      }

      $dir_path = "$this->path/$table_name";
      if ($table_prefix = $this->getTablePrefix($table_name)) {
        $dir_path .= "/$table_prefix";
      }
      pdosnapshot_prepare_directory($dir_path);

      $phases = array(
        'structure' => array($this, 'dumpTableStructure'),
        'content' => array($this, 'dumpTableContent'),
      );
      foreach ($phases as $phase => $callback) {
        $file_path = "$dir_path/$phase";
        $file_info = new SplFileInfo($file_path);

        $this->timingInfoStart('table');
        // Calls $this->dumpTableStructure | $this->dumpTableContent
        $count = call_user_func($callback, $file_info, $table_name, $table_schema);
        $elapsed = $this->timingInfo('table');

        $t_args = array('@table' => $table_name, '@phase' => $phase, '@count' => $count, '@elapsed' => $elapsed);
        $this->log('@phase: @count item / @elapsed s.', $t_args, 'ok');
      }
    }

    // Remove excess directories.
    $dir = new DirectoryIterator($this->path);
    foreach ($dir as $table_dir) {
      /** @var DirectoryIterator $table_dir */
      $table_name = $table_dir->getFilename();
      // There might be a .git in here.
      if (!$table_dir->isDir() || $table_dir->isDot() || substr($table_name, 0, 1) === '.') {
        continue;
      }
      if (!array_key_exists($table_name, $schema)) {
        $dir_path = $table_dir->getRealPath();
        unlink("$dir_path/structure");
        unlink("$dir_path/content");
        // Let this throw an error if other files are present.
        rmdir($dir_path);
      }
    }

    // Release lock.
    $this->timingInfoStart('lock');
    PDOSnapshotConnectionExtender::get($this->connection)->unlockDBForDump();
    $elapsed = $this->timingInfo('lock');
    $this->log('Released lock in @elapsed s.', array('@elapsed' => $elapsed), 'ok');

    $elapsed = $this->timingInfo('all');
    $t_args = array('@total' => $total_count, '@elapsed' => $elapsed);
    $this->log('Dumped @total tables in @elapsed s.', $t_args, 'ok');
  }

  public function dumpTableStructure(SplFileInfo $file_info, $table_name, $table_schema) {
    $serialized = $this->serialize($table_schema);
    $this->writeFileContent($file_info, $serialized);
    return 1;
  }

  public function dumpTableContent(SplFileInfo $file_info, $table_name) {
    $file = $file_info->openFile('w');
    if ($this->isCacheTable($table_name)) {
      return 0;
    }
    $query = db_select($table_name, NULL, array('fetch' => PDO::FETCH_ASSOC))
      ->fields($table_name);
    $record_count = 0;
    foreach ($query->execute() as $record) {
      $this->writeSerializedRecord($file, $record);
      $record_count +=1;
      $this->microlog('+');
    }
    $this->microlog("\n");
    return $record_count;
  }

  public function restore() {
    $this->timingInfoStart('all');
    $exceptions = array();

    // Get lock.
    $this->timingInfoStart('lock');
    PDOSnapshotConnectionExtender::get($this->connection)->lockDBForRestore();
    $elapsed = $this->timingInfo('lock');
    $this->log('Got lock in @elapsed s.', array('@elapsed' => $elapsed), 'ok');

    $previous_table_names = PDOSnapshotConnectionExtender::get($this->connection)->getTableNames();
    $snapshot_dir = new DirectoryIterator($this->path);

    // Iterate over our directory and collect table directories, to get the count.
    $table_dir_paths = array();
    foreach ($snapshot_dir as $table_dir) {
      /** @var DirectoryIterator $table_dir */
      $table_name = $table_dir->getFilename();
      // There might be a .git in here.
      if (!$table_dir->isDir() || $table_dir->isDot() || substr($table_name, 0, 1) === '.') {
        continue;
      }
      $table_dir_path = $table_dir->getPathname();
      $table_dir_paths[$table_name] = $table_dir_path;
    }
    $total_count = count($table_dir_paths);

    // Now process tables.
    $processed_table_names = array();
    foreach ($table_dir_paths as $table_name => $table_dir_path) {
      $processed_table_names[] = $table_name;
      $table_count = count($processed_table_names);

      $t_args = array('@table' => $table_name, '@count' => $table_count, '@total' => $total_count);
      if ($this->checkTablePrefix($table_name)) {
        $this->log('Restoring @count/@total: @table', $t_args, 'ok');
      }
      else {
        $this->log('Skipping due to prefix @count/@total: @table', $t_args, 'ok');
        continue;
      }
      $prefixed_table_dir_path = $table_dir_path;
      if ($prefix = $this->getTablePrefix($table_name)) {
        $prefixed_table_dir_path .= "/$prefix";
      }

      $phases = array(
        'structure' => array($this, 'restoreTableStructure'),
        'content' => array($this, 'restoreTableContent'),
      );
      // Handle PDO restore errors.
      try {
        foreach ($phases as $phase => $callback) {
          $file_name = "$prefixed_table_dir_path/$phase";
          $file_info = new SplFileInfo($file_name);
          while ($file_info->isLink()) {
            $structure_file = $file_info->getLinkTarget();
          }
          assert($file_info->isFile());
          assert($file_info->isReadable());

          // Calls $this->restoreTableStructure | $this->restoreTableContent
          $this->timingInfoStart('table');
          list($count, $row_exceptions) = call_user_func($callback, $table_name, $file_info);
          $exceptions = array_merge($exceptions, $row_exceptions);
          $elapsed = $this->timingInfo('table');

          $t_args = array('@table' => $table_name, '@phase' => $phase, '@count' => $count, '@elapsed' => $elapsed);
          $this->log('@phase: @count item / @elapsed s.', $t_args, 'ok');
        }
      } catch (Exception $e) {
        $message = $e->getMessage();
        $t_args['@message'] = $message;
        $this->log('Exception at @phase of @table: @message', $t_args, 'error');
        $exceptions[] = $e;
      }
    }

    // Drop all excess tables.
    $databaseSchema = $this->connection->schema();
    $excess_table_names = array_diff($previous_table_names, $processed_table_names);

    foreach($excess_table_names as $table_name) {
      $databaseSchema->dropTable($table_name);
    }

    // Release lock.
    $this->timingInfoStart('lock');
    PDOSnapshotConnectionExtender::get($this->connection)->unlockDBForRestore();
    $elapsed = $this->timingInfo('lock');
    $this->log('Released lock in @elapsed s.', array('@elapsed' => $elapsed), 'ok');

    $elapsed = $this->timingInfo('all');
    $exception_count = count($exceptions);
    $t_args = array('@total' => $total_count, '@elapsed' => $elapsed, '@exceptions' => $exception_count);
    $this->log('Restored @total tables in @elapsed s. @exceptions exceptions', $t_args, $exception_count ? 'error' : 'ok');

    return $exceptions;
  }

  protected function restoreTableStructure($table_name, SplFileInfo $file_info) {
    $dump = $this->readFileContent($file_info);
    $data = $this->unserialize($dump);
    $databaseSchema = $this->connection->schema();
    $databaseSchema->dropTable($table_name);
    $databaseSchema->createTable($table_name, $data);
    return array(1, array());
  }

  protected function restoreTableContent($table_name, SplFileInfo $file_info) {
    $record_count = 0;
    $exceptions = array();
    $file = $file_info->openFile();
    while(($record = $this->readRecord($file)) !== FALSE) {
      try {
        $this->connection
          ->insert($table_name)
          ->fields($record)
          ->execute();
      } catch (Exception $e) {
        $message = $e->getMessage();
        $t_args = array('@table' => $table_name, '@count' => $record_count, '@message' => $message);
        $this->log('Exception at restore record @count of @table: @message', $t_args, 'error');
        $this->log('Offending record: @record', array('@record' => print_r($record, 1)), 'error');
        $exceptions[] = $e;
      }
      $record_count +=1;
      $this->microlog('+');
    }
    $this->microlog("\n");
    return array($record_count, $exceptions);
  }

  protected function readFileContent(SplFileInfo $file_info) {
    $file = $file_info->openFile();
    $lines = '';
    while(!$file->eof()) {
      $line = $file->fgets();
      $lines[] = $line;
    }
    $content = implode($lines);
    return $content;
  }

  protected function readRecord(SplFileObject $file) {
    $serialized = $this->readDelimitedString($file);
    if ($serialized) {
      $record = $this->unserialize($serialized);
      return $record;
    }
    else {
      return FALSE;
    }
  }

  protected function readDelimitedString(SplFileObject $file) {
    $lines = array();
    while (!$file->eof() && ($line  = $file->fgets()) && ($line !== "\n")) {
      $lines[] = $line;
    }
    $content = implode($lines);
    return $content;
  }

  protected function writeFileContent(SplFileInfo $file_info, $content) {
    $file = $file_info->openFile('w');
    $file->fwrite($content);
  }

  protected function writeSerializedRecord(SplFileObject $file, $data) {
    $serialized = $this->serialize($data);
    $this->writeDelimitedString($file, $serialized);
  }

  protected function writeDelimitedString(SplFileObject $file, $content) {
    $delimited_content = "$content\n\n";
    $file->fwrite($delimited_content);
  }

  /**
   * Serialize data.
   *
   * Fork of drupal_var_export() with '$' escaping fix from portabledb_var_export()
   * (but not it's removal of object handling).
   *
   * Contract: Caller can assert that the return value has no empty lines.
   * So empty lines can be used as row separator.
   *
   * @param mixed $data
   * @return string
   */
  protected function serialize($var, $prefix = '') {
    if (is_array($var)) {
      if (empty($var)) {
        $output = 'array()';
      }
      else {
        $output = "array(\n";
        // Don't export keys if the array is non associative.
        $export_keys = array_values($var) != $var;
        foreach ($var as $key => $value) {
          $output .= '  ' . ($export_keys ? $this->serialize($key) . ' => ' : '') . $this->serialize($value, '  ', FALSE) . ",\n";
        }
        $output .= ')';
      }
    }
    elseif (is_bool($var)) {
      $output = $var ? 'TRUE' : 'FALSE';
    }
    elseif (is_string($var)) {
      $line_safe_var = str_replace("\n", '\n', $var);
      if (strpos($var, "\n") !== FALSE || strpos($var, "'") !== FALSE) {
        // If the string contains a line break or a single quote, use the
        // double quote export mode. Encode backslash and double quotes and
        // transform some common control characters.
        $var = str_replace(array('\\', '"', "\n", "\r", "\t", '$'), array('\\\\', '\"', '\n', '\r', '\t', '\$'), $var);
        $output = '"' . $var . '"';
      }
      else {
        $output = "'" . $var . "'";
      }
    }
    elseif (is_object($var) && get_class($var) === 'stdClass') {
      // var_export() will export stdClass objects using an undefined
      // magic method __set_state() leaving the export broken. This
      // workaround avoids this by casting the object as an array for
      // export and casting it back to an object when evaluated.
      $output = '(object) ' . drupal_var_export((array) $var, $prefix);
    }
    else {
      $output = var_export($var, TRUE);
    }

    if ($prefix) {
      $output = str_replace("\n", "\n$prefix", $output);
    }

    return $output;
  }

  /**
   * Unserialize data.
   *
   * @param string $dump
   * @return mixed
   */
  protected function unserialize($dump) {
    return eval("return $dump;");
  }

  /**
   * @param $table_name
   * @return bool
   */
  protected function isCacheTable($table_name) {
    if ($this->is_cache_table) {
      return call_user_func($this->is_cache_table, $table_name);
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param string $string
   * @param string[string] $replacements
   * @param string $severity
   */
  protected function log($string, $replacements, $severity) {
    if ($this->logger) {
      call_user_func($this->logger, $string, $replacements, $severity);
    }
  }

  /**
   * @param string $string
   * @param string[string] $replacements
   * @param string $severity
   */
  protected function microlog($string) {
    if ($this->micrologger) {
      call_user_func($this->micrologger, $string);
    }
  }

  /**
   * @param string $name
   * @return float
   */
  protected function timingInfo($name) {
    $elapsed = microtime(TRUE) - $this->timing_info[$name];
    return round($elapsed, 3);
  }

  /**
   * @param string $name
   */
  protected function timingInfoStart($name) {
    $this->timing_info[$name] = microtime(TRUE);
  }

  /**
   * @param $table_name
   * @return bool
   */
  private function checkTablePrefix($table_name) {
    if (isset($this->table_prefixes)) {
      $prefix = $this->getTablePrefix($table_name);
      return in_array($prefix, $this->table_prefixes);
    }
    else {
      return TRUE;
    }
  }

  /**
   * @param $table_name
   * @return mixed
   */
  private function getTablePrefix($table_name) {
    return $this->connection->tablePrefix($table_name);
  }
}
