<?php

/**
 * @file
 * Drush commands PDOSnapshot.
 *
 * This commands dump the drupal database to files in a DB-independent and git-friendly format.
 *
 * @todo write log somewhere
 */

/**
 * Implements hook_drush_command().
 */
function pdosnapshot_drush_command() {
  $items['pdosnapshot-dump'] = array(
    'aliases' => array('psd'),
    'description' => dt('Dump as PDOSnapshot.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'core' => array(7),
    'arguments' => array(
      'connection' => 'DB connection name, optional, defaults to "default".',
    ),
    'options' => array(
      'prefixes' => array(
        'description' => 'Table prefixes that will be processed, comma-separated.',
        'example_value' => 'shared_,site_1,other_connection.',
        'value' => 'optional',
      )
    )
  );
  $items['pdosnapshot-restore'] = array(
    'aliases' => array('psr'),
    'description' => dt('Restore from PDOSnapshot.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'core' => array(7),
    'arguments' => array(
      'connection' => 'DB connection name, optional, defaults to "default".',
    ),
    'options' => array(
      'prefixes' => array(
        'description' => 'Table prefixes that will be processed, comma-separated.',
        'example_value' => 'shared_,site_1,other_connection.',
        'value' => 'optional',
      )
    )
  );
  return $items;
}

function drush_pdosnapshot_dump($connection_name = 'default') {
  drush_log(dt('Setting maintenance mode and clearing cache'), 'ok');
  $old_maintenance_mode = variable_get('maintenance_mode', 0);
  variable_set('maintenance_mode', 1);
  cache_clear_all();

  pdosnapshot_autoload_register();
  if (module_exists('dblog')) {
    // purge dblog entries
    dblog_cron();
  }
  if (module_exists('field')) {
    // First purge deleted fields.
    field_sync_field_status();
    while (
      ($instances = field_read_instances(array('deleted' => 1), array('include_deleted' => 1))) ||
      ($fields = field_read_fields(array('deleted' => 1), array('include_deleted' => 1)))
    ) {
      drush_log(dt('First purging deleted fields'), 'ok');
      field_purge_batch(10);
    }
  }
  // Now do the work.
  $connection = Database::getConnection('default', $connection_name);
  $options = pdosnapshot_options();
  $snapshot = new PDOSnapshot($connection, pdosnapshot_get_path(), $options);
  $snapshot->dump();

  drush_log(dt('Setting back maintenance mode and clearing cache'), 'ok');
  variable_set('maintenance_mode', $old_maintenance_mode);
  cache_clear_all();
  drush_log(dt('Finished dump'), 'ok');
}

function drush_pdosnapshot_restore_init() {
  global $pdosnapshot_bootstap_error;
  // Only set maintenance mode if we can bootstrap to full.
  // This must be tried in init.
  try {
    drush_log(dt('Trying to bootstrap drupal'), 'ok');
    $success = drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL);
    $pdosnapshot_bootstap_error = $success ? NULL : drush_get_error();
  }
  catch (Exception $e) {
    $pdosnapshot_bootstap_error = $e->getMessage();
  }
}

function drush_pdosnapshot_restore($connection_name = 'default') {
  global $pdosnapshot_bootstap_error;
  if (!$pdosnapshot_bootstap_error) {
    drush_log(dt('Setting maintenance mode and clearing cache'), 'ok');
    $old_maintenance_mode = variable_get('maintenance_mode', 0);
    variable_set('maintenance_mode', 1);
    cache_clear_all();
  }
  else {
    drush_log(dt('Drupal not bootstrapped: @error', array('@error' => $pdosnapshot_bootstap_error)), 'ok');
  }

  pdosnapshot_autoload_register();
  // Bootstrap to database contains cache so won't work with empty DB, so we boot to configuraiton.
  // Boot DB ourself,
  _drupal_bootstrap_database();
  // \DatabaseSchema_sqlite::processField() calls drupal_strtoupper().
  require_once DRUPAL_ROOT . '/includes/unicode.inc';

  $connection = Database::getConnection('default', $connection_name);
  $options = pdosnapshot_options();
  $snapshot = new PDOSnapshot($connection, pdosnapshot_get_path(), $options);
  $snapshot->restore();
  if ($pdosnapshot_bootstap_error) {
    drush_log(dt('Setting back maintenance mode and clearing cache'), 'ok');
    variable_set('maintenance_mode', $old_maintenance_mode);
    cache_clear_all();
  }
  drush_log(dt('Finished restore'), 'ok');
}

function pdosnapshot_options() {
  $table_prefixes = drush_get_option('prefixes');
  if (isset($table_prefixes)) {
    // We do it here, as drush_get_option_list() coughs on NULL defaults.
    $table_prefixes = explode(',', $table_prefixes);
  };
  return array(
    'logger' => function($string, $replacements, $severity) {
        drush_log(dt($string, $replacements), $severity);
      },
    'micrologger' => function($string) {
        fwrite(STDERR, $string);
      },
    'is_cache_table' => function($table_name) {
        // @todo add watchdog, sessions
        return substr($table_name, 0, 5) === 'cache'
          || substr($table_name, 0, 7) === 'advagg_';
      },
    'table_prefixes' => $table_prefixes,
  );
}

function pdosnapshot_autoload_register() {
  spl_autoload_register(function($class) {
    include dirname(__FILE__) . "/classes/$class.php";
  });
}

function pdosnapshot_get_path() {
  static $path;
  if (!isset($path)) {
    // usually sites/SITE/pdosnapshot, @todo error handling
    $path = DRUPAL_ROOT . '/' . conf_path() . '/pdosnapshot';
    pdosnapshot_prepare_directory($path);
    pdosnapshot_create_htaccess($path);
  }
  return $path;
}

function pdosnapshot_prepare_directory($path) {
  // file_prepare_directory() needs a higher bootstrap level.
  if (!is_dir($path)) {
    mkdir($path, 0775, TRUE);
  }
  chmod($path, 0775);
}

function pdosnapshot_create_htaccess($path) {
  // file_create_htaccess() needs a higher bootstrap level.
  $htaccess_path =  $path . '/.htaccess';
  if (file_exists($htaccess_path)) {
    // Short circuit if the .htaccess file already exists.
    return;
  }
  $htaccess_lines = "SetHandler Drupal_Security_Do_Not_Remove_See_SA_2006_006\nDeny from all\nOptions None\nOptions +FollowSymLinks";
  if (file_put_contents($htaccess_path, $htaccess_lines)) {
    chmod($htaccess_path, 0444);
  }
  else {
    // @todo handle error
  }
}
