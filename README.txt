PDOSnapshot
===========

Provides drush commands
* pdosnapshot-dump
* pdosnapshot-restore

These commands map the DB content to a git-friendly database-independent format in the filesystem.
Its location defaults to sites/SITE/files/pdosnapshot (.htaccess protected).
Each table corresponds to a directory, containing files named structure and content.

Known issues:
* Code definitely needs some cleanup and comments.
* DB locking. See below.
* Exceptions are caught and printed out, but this might go into some kind of log in a file.
* Table prefixing (for multisites with partially shared and partially prefixed tables) is prepared but currently untested.
* "field_data_deleted_foo" tables completely break restore, as they have no schema information.
PDOSnapshot does its best to trigger field table deletion before a dump so no such tables should be left over.

DB locking
==========
Typical execution time for dump is 10sec, for restore 10min.
The current implementation wraps the whole dump or restore in a transaction, and sets drupal maintenance_mode.

Dump optimization
-----------------
All we need is to have a snapshot of the database that does not change.
For MySQL a transaction should be enough to guarantee this IF no DDL statements are executed.
So we might want to extend the drupal dtabase driver with a "No DDL / Schema changes" mode.
On AWS a DB snapshot can be used, or ZFS a file snapshot.

So if we can dump without maintenance_mode, we can even use PDOSnapshots for regular DB backup.


Restore optimization
--------------------
On a restore DB is overwritten anyway, so no need to optimize.
We might check data and only write changes if speed matters.